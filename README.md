documentation-drupal-with-nginx-D7-branch
=========================================


# THIS IS NO LONGER MAINTAINED


Documentation for perusio's drupal-with-nginx D7 branch, developed for my personal use.  You may find it helpful.

Perusio's [configuration](https://github.com/perusio/drupal-with-nginx) is masterful - a great learning tool, security conscious and adaptable to many situations. I am very grateful for his work. It has saved me a tremendous amount of time and provided me with a working knowledge of nginx, Drupal and PHP-FPM.  I have been studying his configuration and these are documents I developed for my personal use so I could better understand his configuration.  I work only with the D7 branch so some of this may or may not be applicable to the D6 branch. 
